package com.nl2go.playground.ipconfig.controllers.services;

import com.nl2go.playground.ipconfig.controllers.models.IpConfig;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Arrays.asList;

@Service
public class IpConfigMockService {

    public List<IpConfig> getIpConfigs(){
        return asList(
                new IpConfig("127.0.0.1", "mta-prod-1", true),
                new IpConfig("127.0.0.2", "mta-prod-1", false),
                new IpConfig("127.0.0.3", "mta-prod-2", true),
                new IpConfig("127.0.0.4", "mta-prod-2", true),
                new IpConfig("127.0.0.5", "mta-prod-2", false),
                new IpConfig("127.0.0.6", "mta-prod-3", false)
        );
    }
}
