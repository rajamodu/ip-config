FROM rajamodu/javabase:ip-config
WORKDIR /opt/app
ADD target/ip-config-0.0.1-SNAPSHOT.jar /opt/app
EXPOSE 8080
CMD ["/usr/bin/supervisord"]